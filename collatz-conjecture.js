const isEven = number => number % 2 === 0;

const collatz = number => {
    let n = number;
    let steps = 0;
    while (n > 1) {
        steps = steps + 1;
        
        n = isEven(n) ? (n / 2) : (3 * n + 1);
    }
    console.log(steps);
};

collatz(12);