// {number}
// Add -> {number} plus/PLUS {number}
// Minus -> {number} minus/MINUS {number}
// multiplied -> {number} multiplied by/MULTIPLIED BY {number}
// divided -> {number} divided/DIVIDED {number}
// Exponentials -> {number} raised to the {number}th power
// Multiple: {number} plus {number} multiplied by {number}
// Handle a set of operations, in sequence -> evaluate the expression from left-to-right

const { evaluate } = require('mathjs');
const containNumberInStringRegex = /\d+/;
const operationSpliceRegex = /(\d+)(\s(plus|minus|multiplied by|divided by|raised to the)\s\d+(th\spower)?)+/gi;

const parseWords = string => {
    const checkExitNumber = containNumberInStringRegex.test(string);
    const extractResult = string.match(operationSpliceRegex);
    if (!checkExitNumber || !extractResult) {
        console.log('Cannot make operation!');
    } else {
        let operationInString = extractResult[0];
        operationInString = operationInString.replace(/plus/g, '+')
        .replace(/minus/, '-')
        .replace(/multiplied by/g, '*')
        .replace(/divided by/g, '/')
        .replace(/raised to the/g, '^')
        .replace(/th power/g, '');
        const result = evaluate(operationInString);
        console.log('Evaluate: ', result);
    }
};

parseWords('What is 5?');
parseWords('What is 5 plus 13?');
parseWords('What is 7 minus 5?');
parseWords('What is 6 multiplied by 4?');
parseWords('What is 25 divided by 5?');
parseWords('What is 5 plus 13 plus 6?');
parseWords('What is 3 plus 2 multiplied by 3?');
parseWords('What is 2 raised to the 5th power?');
parseWords('What is 2 raised to the 5th power plus 2?');

// In valid
parseWords('What is 52 cubed?');
parseWords('Who is the President of the United States');
parseWords('What is 1 plus plus 2?');
parseWords('What is 1 minus plus 2?');
parseWords('What is 1 minus divided by 2?');
